<?php

/**
 * @file
 * Hooks provided by the Jstimer module.
 */

/**
 * Builds the JS for a widget.
 *
 * @see jst_clock_jstwidget().
 * @see jst_timer_jstwidget().
 */
function hook_jstwidget() {
  // No example.
}
